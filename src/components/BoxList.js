import React from 'react';
const BoxItem = (props) => {
    return (

        <li className="yellow">
            {props.propsBox_child}
        </li>
    )
};

const BoxList = (props) => {
    const boxList=props.boxList;
    
    return (
      
        <ul className="box mtb-15">
            {boxList.map((itemBox) =>
                <BoxItem propsBox_child={itemBox} />
            )};

        </ul>
    )
}

export default BoxList;