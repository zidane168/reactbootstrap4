import React from 'react';

const Headline = (props) => {
    return (
        <div>
            <h1> {props.bigTitle}  </h1>
            <h2> {props.children} </h2>
        </div>
    )
}

export default Headline;
