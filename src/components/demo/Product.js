

import { Component } from "react";
import './Product.css';

class Product extends Component {

    buy = () => { // dung cach nay ()=> de biet dc who is click
        alert(this.props.children); 
    };

    render() {
        return (
            <div className="col-md-4 ">
                <div className="card text-center p-10">
                    <img src={ this.props.image } alt="1.jpg" className="margin-auto img-responsive width-100" />
                    <div className="card-body">
                    <h4 className="card-title red"> { this.props.children } </h4>
                    <p className="card-text"> { this.props.info }</p>
                    <p className="card-text lbl-danger"> { this.props.price } </p>
                    <a className="btn btn-success text-color-white bold" onClick={this.buy}>  BUY </a>
                    </div>
                </div>
            </div>
        );
    }
};

export default Product;