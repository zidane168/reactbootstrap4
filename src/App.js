
import './App.css';

import Header   from './components/Header';
import Headline from './components/Headline';
import Aside    from './components/Aside';
import Footer   from './components/Footer';
import ItemList from './components/ItemList';
import BoxList  from './components/BoxList';
import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

import { Button } from 'react-bootstrap';

import Alert from 'react-bootstrap/Alert';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = blog;
  };

  render() {
    return (
      <div id="layout" className="container  ">

        <div className="row mtb-15">
          <div className="col-md-12  green">
            <Header/>
          </div>
        </div>


        <div className="row mtb-15">
          <div className="col-md-12">
      
            <Alert dismissible variant="danger">
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>
                Change this and that and try again.
              </p>
            </Alert>

          </div>
        </div>
  
        <div className="row">
          <div className="col-md-8 grey">
            <div className="row">
              <section id="">
                <Headline  bigTitle={this.state.bigTitle}> Children?? </Headline>
                <ItemList itemList={this.state.itemList}/>
      
              </section>
            </div>

            <div className="row">
              <div className="col-md-12">
                <button className="btn btn-success" type="button" onClick={this.changeItem}> Change </button>
              </div>
            </div>

          </div>

          <div className="col-md-4 pink">
           <Aside> </Aside>
          </div>
        </div>


        <BoxList boxList={this.state.boxList}/>
      
        <div className="row">
          <div className="col-12 grey">
            <Footer />
          </div>
        </div>
      </div>
    );
  };

  
  changeItem = () => {
    this.setState({
      itemList: [
        {
          title: 'Title Changed',
          info: 'wjay theeeee ????'
        },
        {
          title: 'Title Changed 2',
          info: 'wjay theeeee !!!!'
        }
      ]
    });
  };
};

const blog = {
  bigTitle: 'What is big title ^_^',
  itemList: [
    {
      title: 'Title 1',
      info: 'This is title one, summary'
    },
    {
      title: 'Title 2',
      info: 'This is title two, This is title one, This is title one'
    },
    {
      title: 'Title 3',
      info: 'This is title three, This is title one, This is title one'
    }
  ],

  boxList: [
    'BoxList number 1',
    'BoxList number 2',
    'BoxList number 3',
    'BoxList number 4',
  ]
};

export default App;


