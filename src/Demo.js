import './Demo.css';

import Header from './components/demo/Header';
import Product from './components/demo/Product';

import React, { Component } from 'react';
import reactDom from 'react-dom';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            products: [
                {
                    id: 1,
                    name: 'Xiao mi 5 Pro',
                    info: 'Ram: 3GB/64',
                    price: 2000000,
                    image: 'img/1.jpg',
                    status: true,
                },
                {
                    id: 2,
                    name: 'Xiao mi 6 International',
                    info: 'Ram: 4GB/128',
                    price: 3200000,
                    image: 'img/2.jpg',
                    status: true,
                },
                {
                    id: 3,
                    name: 'Xiao mi note 10',
                    info: 'Ram: 8GB/512',
                    price: 4900000,
                    image: 'img/3.jpg',
                    status: true,
                },
            ],

            isActive: true,
            style: 'btn btn-success',
        };
    };

    // componentWillMount() {

    //     console.log('componentWillMount');

    //     if (localStorage && localStorage.getItem('isActive') && localStorage.getItem('style')) {
            
    //         this.state.isActive = localStorage.getItem('isActive');
    //         this.state.style = localStorage.getItem('style');
            
    //         this.setState(this.state);

    //     } else {
    //         localStorage.setItem('isActive', this.state.isActive);
    //         localStorage.setItem('style', this.state.style);
    //     }
    
    // };

    changeStatus = () => {

        let isActive = this.state.isActive;
        this.state.isActive = !isActive;

        if (this.state.isActive) {
            this.state.style    = 'btn btn-success';
        } else {
            this.state.style    = 'btn btn-danger';
        }
        
        this.setState(this.state);
    };

    render() {
        let products = this.state.products.map((pro, index) => {
            let result = "";

            if (pro.status) {
                result = <Product
                    key={ pro.id }
                    price={ pro.price }
                    image={ pro.image }
                > 
                    { pro.name }
                </Product>
            }

            return result;
        });

        return (
            <div className="container">
                <Header></Header>
                <div className="row  mt-15">
                    { products }                     
                </div>

                <button className={ this.state.style }  onClick={ this.changeStatus }> {this.state.isActive === true ? 'true' : 'false'} </button>
            </div>
        );  
    };

};

export default App;


